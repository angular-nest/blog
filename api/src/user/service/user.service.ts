import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { from, Observable, throwError } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';
import { AuthService } from 'src/auth/services/auth.service';
import { Repository } from 'typeorm';
import { UserEntity } from '../models/user.entity';
import { User } from '../models/user.interface';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
    private authService: AuthService,
  ) {}
  create(user: User): Observable<User> {
    return this.authService.hashPassword(user.password).pipe(
      switchMap((passwordHash: string) => {
        const newUser = new UserEntity();
        newUser.name = user.name;
        newUser.username = user.username;
        newUser.email = user.email;
        newUser.password = passwordHash;
        return from(this.userRepository.save(newUser)).pipe(
          map((user: User) => {
            delete user.password;
            return user;
          }),
          catchError(err => throwError(err)),
        );
      }),
    );
  }
  findOne(id: number): Observable<User> {
    return from(this.userRepository.findOne(id));
  }
  findAll(): Observable<User[]> {
    return from(this.userRepository.find());
  }
  deleteOne(id: number): Observable<any> {
    return from(this.userRepository.delete(id));
  }
  updateOne(id: number, user: User): Observable<any> {
    delete user.email;
    delete user.password;
    return from(this.userRepository.update(id, user));
  }
  login(user: User): Observable<string> {
    return this.validateUser(user.email, user.password).pipe(
      switchMap((user: User) => {
        if (user) {
          return this.authService
            .generateJWT(user)
            .pipe(map((jwt: string) => jwt));
        } else {
          return 'Wrong Credentials';
        }
      }),
    );
  }
  validateUser(email: string, password: string): Observable<User> {
    return this.findByMail(email).pipe(
      switchMap((user: User) => {
        // console.log(user);
        return this.authService.comparePasswords(password, user.password).pipe(
          map((match: boolean) => {
            if (match) {
              delete user.password;
              return user;
            } else {
              throw Error;
            }
          }),
        );
      }),
    );
  }
  findByMail(email: string): Observable<User> {
    return from(this.userRepository.findOne({ email })).pipe(
      switchMap((user: User) => {
        return from(
          this.userRepository.findOne({
            where: {
              email: email,
            },
            select: ['password'],
          }),
        ).pipe(
          map((userWithPass: User) => {
            // console.log(userWithPass);
            user.password = userWithPass.password;
            return user;
          }),
        );
      }),
    );
  }
}
