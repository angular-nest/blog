# Blog

1. Setup

- Use git and gitlab
- Api setup with nestjs
- Postgres Database Elastic Search
- .env file

2. Create Working User api
   Acceptance Criteria:

- Use typeorm and the repository from typeorm
- Use Observables instead of Promises (use rxjs)
- Feature Module "user"
- user should have properties
  -- name
  -- username (unique)
  -- id (unique, primary key)
- use git flow

3. Add JWT and Login Endpoint

- New Endpoint: POST '/login', check password in method
- Expand User Model with email, password
- Expand 'create' Endpoint
- Store 'email' always in lowercase in database
- Store 'password' always as hashed value in database
- add a Auth Module for this
